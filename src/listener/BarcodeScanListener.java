package listener;

import domain.Barcode;

public interface BarcodeScanListener {
    void onBarcodeScan(Barcode barcode);
}
