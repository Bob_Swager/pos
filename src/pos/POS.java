package pos;

import forms.BarcodeScannerForm;
import forms.LCDDisplayForm;
import forms.PrinterForm;
import forms.MainForm;

public class POS {

    public static void main(String[] args) {
        BarcodeScannerForm scanner = new BarcodeScannerForm();
        LCDDisplayForm display = new LCDDisplayForm();
        PrinterForm printer = new PrinterForm();
        MainForm m = new MainForm(display, printer, scanner);
        m.show();
    }

}
