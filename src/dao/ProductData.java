package dao;

import domain.Barcode;
import domain.Product;

public class ProductData {

    public void ProductData() {
    }

    public Product[] getProducts() {
        Product[] listProducts = new Product[5];
        listProducts[0] = new Product(Long.parseLong("1"), "cherry", new Barcode("1"), 1.3);
        listProducts[1] = new Product(Long.parseLong("2"), "orange", new Barcode("2"), 1.4);
        listProducts[2] = new Product(Long.parseLong("3"), "apple", new Barcode("3"), 1.5);
        listProducts[3] = new Product(Long.parseLong("4"), "potato", new Barcode("4"), 1.6);
        listProducts[4] = new Product(Long.parseLong("5"), "tomato", new Barcode("5"), 1.7);
        return listProducts;
    }
}
