package dao;

import domain.Barcode;
import domain.Product;

public interface ProductDAO {
    Product getByBarcode(Barcode barcode);
}
