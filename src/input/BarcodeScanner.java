package input;

import domain.Barcode;
import domain.Product;
import dao.ProductDAO;

public class BarcodeScanner {

    private final ProductDAO productRepository;

    public BarcodeScanner(ProductDAO productRepository) {
        this.productRepository = productRepository;
    }

    public Product scan(Barcode barcode) {
        String code = barcode.getCode();
        if (!checkNotEmpty(code)) {
            System.out.print("Invalid barcode: " + code);
        }
        return productRepository.getByBarcode(barcode);
    }

    public static boolean checkNotEmpty(String string) {
        return string != null && !string.isEmpty();
    }
}
