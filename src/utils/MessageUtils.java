package utils;

import domain.ProductInReceipt;

public class MessageUtils {

    public static String getProductMessage(ProductInReceipt product){
        double sum = Math.floor((product.getProduct().getPrice()*product.getCount())* 100) / 100;
        return new StringBuilder()
                .append(product.getProduct().getName())
                .append("\t")
                .append(product.getProduct().getPrice())
                .append(" $")
                .append("\t")
                .append(product.getCount())
                .append("\t")
                .append(sum)
                .append("$")
                .toString();
    }

    public static String getTotalSumMessage(Double sum){
        return "Total sum: " + sum.toString() + "$";
    }
}
