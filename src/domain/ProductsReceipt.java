package domain;

import java.util.ArrayList;
import java.util.List;

public class ProductsReceipt implements Receipt<Product> {

    private final List<ProductInReceipt> products = new ArrayList<>();
    private Double sum = 0.0;

    @Override
    public void add(ProductInReceipt product) {
        products.add(product);
        sum += product.getProduct().getPrice() * product.getCount();
    }

    @Override
    public Double getSum() {
        sum = 0.0;
        products.stream().forEach((product) -> {
            sum += product.getProduct().getPrice() * product.getCount();
        });
        return Math.floor(sum * 100) / 100;
    }

    @Override
    public List<ProductInReceipt> getAll() {
        return products;
    }

    public ProductInReceipt getProduct(Barcode barcode) {
        ProductInReceipt returnedProduct = null;
        for (ProductInReceipt product : products) {
            if (product.getProduct().getBarcode() == barcode) {
                returnedProduct = product;
            }
        }
        return returnedProduct;
    }

}
