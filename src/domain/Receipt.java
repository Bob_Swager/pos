package domain;

import java.util.List;

public interface Receipt<T> {

    void add(ProductInReceipt p);

    Double getSum();

    List<ProductInReceipt> getAll();
}
