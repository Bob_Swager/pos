package domain;

import dao.ProductDAO;

public class Product implements ProductDAO {

    private Long id;
    private String name;
    private Barcode barcode;
    private Double price;

    public Product() {
    }

    public Product(Long id, String name, Barcode barcode, double price) {
        this.id = id;
        this.name = name;
        this.barcode = barcode;
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Barcode getBarcode() {
        return barcode;
    }

    public Double getPrice() {
        return price;
    }

    @Override
    public Product getByBarcode(Barcode barcode) {
        return this;
    }
}
