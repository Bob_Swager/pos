package domain;

public class ProductInReceipt {

    private final Product product;
    private int count;
    private final int position;

    public ProductInReceipt(int position, Product product, int count) {
        this.position = position;
        this.product = product;
        this.count = count;
    }

    public Product getProduct() {
        return product;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getPosition() {
        return position;
    }
}
