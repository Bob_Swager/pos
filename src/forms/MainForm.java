package forms;

import dao.ProductData;
import domain.Barcode;
import domain.Product;
import domain.ProductInReceipt;
import listener.BarcodeScanListener;
import domain.ProductsReceipt;
import java.util.List;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import utils.MessageUtils;

public class MainForm extends javax.swing.JFrame implements BarcodeScanListener {

    public MainForm() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        totalLabel = new javax.swing.JLabel();
        totalTextField = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Application");

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "NAME", "PRICE", "COUNT"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.Double.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.setColumnSelectionAllowed(true);
        jTable1.setName(""); // NOI18N
        jTable1.setRequestFocusEnabled(false);
        jScrollPane1.setViewportView(jTable1);
        jTable1.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);

        totalLabel.setText("Total: ");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(totalLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(totalTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 452, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(totalLabel)
                    .addComponent(totalTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainForm().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JLabel totalLabel;
    private javax.swing.JTextField totalTextField;
    // End of variables declaration//GEN-END:variables

    public static final String INVALID_BAR_CODE = "Invalid bar-code";
    public static final String PRODUCT_NOT_FOUND = "Product not found";
    public static final String EXIT_CODE = "exit";
    int productPosition;
    private BarcodeScannerForm scanner;
    private LCDDisplayForm display;
    private PrinterForm printer;
    private ProductsReceipt receipt = getNewProductReceipt();

    public MainForm(LCDDisplayForm display, PrinterForm printer, BarcodeScannerForm scanner) {
        initComponents();
        this.scanner = scanner;
        this.display = display;
        this.printer = printer;
        productPosition = 0;
        this.show();

        jTable1.getModel().addTableModelListener((TableModelEvent e) -> {
            ProductInReceipt productInReceipt = null;
            if (e.getType() == TableModelEvent.UPDATE) {
                for (ProductInReceipt product : receipt.getAll()) {
                    if (product.getPosition() == e.getFirstRow() + 1) {
                        int count = Integer.parseInt(jTable1.getModel().getValueAt(e.getFirstRow(), e.getColumn()).toString());
                        product.setCount(count);
                        productInReceipt = new ProductInReceipt(product.getPosition(), product.getProduct(), count);
                    }
                }
                totalTextField.setText(receipt.getSum().toString());        
                String message = MessageUtils.getProductMessage(productInReceipt);
                display.showMessage(message);            
            }
        });
        showDevices();
    }

    private void showDevices() {
        scanner.show();
        display.show();
        display.showMessage("no item");
    }

    @Override
    public void onBarcodeScan(Barcode barcode) {
        if (isInvalidCode(barcode)) {
            handleInvalidCode();
        } else {
            handleValidCode(barcode);
        }
    }

    private void handleValidCode(Barcode barcode) {
        if (isExitCode(barcode.toString())) {
            handleExitCode();
        } else {
            Product product = getProductByBarcode(barcode);
            handleScannedProduct(product);
        }
    }

    private Product getProductByBarcode(Barcode barcode) {
        ProductData productData = new ProductData();
        Product returnProduct = null;
        Product[] listProducts = productData.getProducts();
        for (Product p : listProducts) {
            if (p.getBarcode().equals(barcode)) {
                returnProduct = p;
            }
        }
        return returnProduct;
    }

    private void handleScannedProduct(Product product) {
        if (product != null) {
            handleProductFound(product);
        } else {
            handleProductNotFound();
        }
    }

    private void handleProductNotFound() {
        display.showMessage(PRODUCT_NOT_FOUND);
    }

    private void handleProductFound(Product product) {
        int position = ++productPosition;
        ProductInReceipt productInReceipt = new ProductInReceipt(position, product, 1);
        String message = MessageUtils.getProductMessage(productInReceipt);
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        model.addRow(new Object[]{product.getId(), product.getName(), product.getPrice(), 1});
        receipt.add(productInReceipt);
        display.showMessage(message);
        totalTextField.setText(receipt.getSum().toString());
    }

    private void handleInvalidCode() {
        display.showMessage(INVALID_BAR_CODE);
    }

    private boolean isInvalidCode(Barcode barcode) {
        return barcode == null || barcode.toString().isEmpty();
    }

    private boolean isExitCode(String barcode) {
        return EXIT_CODE.equals(barcode);
    }

    public void handleExitCode() {
        List<ProductInReceipt> listProducts = receipt.getAll();
        StringBuilder printReceiptText = new StringBuilder();
        for (ProductInReceipt product : listProducts) {
            printReceiptText.append(MessageUtils.getProductMessage(product));
            printReceiptText.append("\n");
        }
        Double sum = receipt.getSum();
        printReceiptText.append(MessageUtils.getTotalSumMessage(sum));
        printer.show();
        printer.printLine(printReceiptText.toString());
        display.showMessage(MessageUtils.getTotalSumMessage(sum));
        receipt = getNewProductReceipt();
    }

    private ProductsReceipt getNewProductReceipt() {
        return new ProductsReceipt();
    }

    CellEditorListener ChangeNotification = new CellEditorListener() {
        @Override
        public void editingCanceled(ChangeEvent e) {
            System.out.println("The user canceled editing.");
        }

        @Override
        public void editingStopped(ChangeEvent e) {
            System.out.println("The user stopped editing successfully.");
        }

    };
}
